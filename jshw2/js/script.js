/*
1.-Числа(number)
  -строки(string)
  -bool(true/false)
  -null(говорит что в переменной значения нет)
  -undefined(неопределенный тип даных)
2.  Строгое и нестрогое сравнеине.
3.Оператор — это элемент языка, задающий полное описание действия, которое необходимо выполнить.
*/
"use strict";

let userName;
let userAge;
let middleAgeResult;

do {
  userName = prompt("Your name?", userName);
} while (userName.trim === "" || !isNaN(userName) || null);

do {
  userAge = prompt("Your age?");
} while (isNaN(+userAge));

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
} else {
  middleAgeResult = confirm("Are you sure you want to continue?");

  if (middleAgeResult) {
    alert(` Welcome, ${userName} `);
  } else alert("You are not allowed to visit this website");
}

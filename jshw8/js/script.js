/*
1.DOM-обєктна модель документа,яка представляє весь вміст сторінки у вигляді обєктів,які можна міняти.
2.innerText - Відображає весь текстовий вміст, який не є синтаксисом HTML.
  innerHTML - покаже текстову інформацію для рівно одного елемента. Вивід включатиме текст і розмітку документа HTML.
3.Звернутися можно за допомогою різних методів.
  document.getElementById() - повертає об'єкт Element за його id.
  document.getElementsByName() - повертає масив елементів за name.
  document.getElementsByTagName() - масив елемінтів за назвою.
  document.getElementsByClassName() - повертає масив елементів за вказаним класом.
  document.querySelector() - повертає об'єкт Element за вказаним CSS селектором.
  document.querySelectorAll() - повертає масив елементів за вказаним CSS селектором.Є самим уріверсальним методом пошуку.
*/
"use strict";

const tagP = document.getElementsByTagName("p");
for (let i = 0; i < tagP.length; i++) {
  tagP[i].style.backgroundColor = "#ff0000";
}
const tagId = document.querySelector("#optionsList");
console.log(tagId);

const idParent = optionsList.parentNode;
console.log(idParent);

const child = optionsList.childNodes;
console.log(child);

const textElement = document.getElementById("testParagraph");
textElement.textContent = `This is a paragraph`;

const header = document.getElementsByClassName("main-header")[0];
const headerChild = header.children;
for (let i = 0; i < headerChild.length; i++) {
  headerChild[i].classList.add("nav-item");
  console.log(headerChild[i]);
}
const sectionClass = document.querySelectorAll(".section-title");
for (let i = 0; i < sectionClass.length; i++) {
  sectionClass[i].classList.remove("section-title");
  console.log(sectionClass[i]);
}

/*
1.Метод обєкту це функція яка належить обєкту.
2.Значення можуть бути будь-якого типу.
3. Посилальний тип – це внутрішній тип мови.
Читання властивості крапкою повертає не саме значення властивості, але спеціальне значення “посилального типу”, яке зберігає як значення властивості, так і об’єкт, з якою він був взятий.
*/
"use strict";

function createNewUser() {
  const newUser = {
    firstName: prompt("firstName"),
    lastName: prompt("lastName"),
    getLogin: function () {
      return (
        newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase()
      );
    },
  };
  console.log(newUser.getLogin());
}
createNewUser();

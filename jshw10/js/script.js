"use strict";

const tabs = document.querySelectorAll(".tabs_title");
const tabsItem = document.querySelectorAll(".tabs_item");

tabs.forEach(function (item) {
  item.addEventListener("click", function () {
    let currentTabBtn = item;
    let tabId = currentTabBtn.getAttribute("data-tab");
    let currentTab = document.querySelector(tabId);

    tabs.forEach(function (item) {
      item.classList.remove("active");
    });

    tabsItem.forEach(function (item) {
      console.log(item);
      item.classList.remove("active");
    });

    currentTabBtn.classList.add("active");
    currentTab.classList.add("active");
  });
});

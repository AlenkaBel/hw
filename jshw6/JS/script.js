/*
1.Екранування символів в програмуванні — заміна в тексті керуючих символів на відповідні текстові підстановки.
2.Function definition,function declaration,function statement) 
3 Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду. Однією з переваг підйому є те, що він дозволяє нам використовувати функції перед їх оголошенням у коді.
*/

"use strict";

let newUser = new createNewUser();
let userAge = newUser.getAge();

function createNewUser() {
  let firstName = prompt("Firstname");
  let secondName = prompt("Secondname");
  let birthday = prompt("Enter your birthdate");

  return {
    getAge: function () {
      let age =
        Date.now() - Date.parse(birthday.split(".").reverse().join("."));
      return Math.floor(age / (1000 * 60 * 60 * 24 * 365).toFixed());
    },

    getPassword: function () {
      return (
        firstName[0].toUpperCase() +
        secondName.toLowerCase() +
        parseInt(birthday.substr(6, 9))
      );
    },
  };
}
console.log(newUser.getPassword());
console.log(newUser.getAge());

/*
1.З допомогою appendChild() мі можемо створити новий тег.
2.Визначає положення елемента додавання відносно елемента, який викликав метод. Повинні відповідати одному з таких значень (з урахуванням регістру):

'beforebegin': до самого елемента (до початкового тегу).
'afterbegin': відразу після елемента, що відкриває тег (перед першою дитиною).
'beforeend': перед міткою закриття елемента (після останньої дитини).
'afterend': after element (після закриття теґу).

3.Для видалення елемента можна застосувати метод remove() 
*/
const defaultArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const generateList = function (items, parent = document.body) {
  let stack = items;
  let root = document.createElement("ul");

  while (stack.length) {
    let item = stack.shift();
    let ul = item.ul || root;
    let value = item.value != null ? item.value : item;
    let li = document.createElement("li");

    if (!Array.isArray(value)) {
      li.innerText = value;
    }
    ul.appendChild(li);
  }
  parent.appendChild(root);
  return root;
};
generateList(defaultArr);

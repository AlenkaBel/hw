/*
1.Метод forEach дозволяє послідовно перебрати всі елементи масиву. Метод в параметрі отримує функцію, яка буде працювати для кожного елемента масиву.
2.Щоб очистити масив від елементів, встановіть його довжину до нуля.
3.Використовувати метод Array. isArray() , щоб підтвердити, що масив є масивом. Цей метод визначає, чи передане є масивом. Якщо передане є масивом, цей метод поверне true .
*/
"use strict";
let toFilter = ["hello", "world", 23, "23", null];

function filterBy(arr, type) {
  let result = arr.filter((i) => typeof i != type);
  return result;
}

const newArr = filterBy(toFilter, "string");
console.log(newArr);
